sap.ui.define([
  "sap/ui/core/Control"
], function(Control) {
  "use strict";

var MultipleToggleButtons = Control.extend("zhcm_Hap_650.CustomControls.MultipleToggleButtons", {
  metadata: {
    properties: {
      scale:          {type: "Object"},
      displayText:      {type: "Boolean", defaultValue: true},
      enabled:        {type: "Boolean", defaultValue: true},
      cantEstimateAvailable:  {type: "Boolean", defaultValue: true},
      cantEstimateVal:    {type: "Boolean"},
      value:          {type: "string"},
      spacing:        {type: "number", defaultValue: 32} //px

    },
    aggregations: {
      _buttons: { type: "sap.m.ToggleButton", multiple: true },
      _nButton: { type: "sap.m.ToggleButton", multiple: false }
    }
  },

  _onButtonPress: function(oEvt){
    var oSource = oEvt.getSource(),
      oTopControl = oSource.getParent(),
      aButtons = oTopControl.getAggregation("_buttons"),
      oNButton = oTopControl.getAggregation("_nButton");

    if(oNButton){
      aButtons.push(oNButton);
    }

    $.each(aButtons, function(sIndex, oButton){
      if(oButton.sId !== oSource.sId){
        oButton.setPressed(false);
      } else {
          if (oButton.getPressed() === true) {
            oTopControl.setValue(oButton.data().valToSet);
            if(oButton.data() !== "0000" && oTopControl.getCantEstimateAvailable()) {
                oTopControl.setCantEstimateVal(false);
            }
          } else {
            oTopControl.setValue("0000");
          }
      }
    });

  },

  _onCantEstimatePress: function(oEvt){
    var p = oEvt.getSource().getParent();
    this._onButtonPress.apply(this, [oEvt]);
    p.setCantEstimateVal(!p.getCantEstimateVal());
  },

  onBeforeRendering: function(){
    if(this.getAggregation("_buttons")){
      return;
    }

    if(this.getCantEstimateAvailable()){
      var oNButton = new sap.m.ToggleButton({
        text: "N",
        pressed: this.getCantEstimateVal(),
        press: this._onCantEstimatePress.bind(this)
      });
      oNButton.addStyleClass("zhcm_Hap_650RatingToggleDesktop");
      oNButton.data("valToSet", "0000");
      this.setAggregation("_nButton", oNButton);
    }

    var oScaleVals = this.getScale(),
      that = this,
      nCurrentMark = this.getValue();

    // this.destroyAggregation("_buttons");

    $.each(oScaleVals, function(sIndex, oScale){
      var oButton = new sap.m.ToggleButton({
        text: parseInt(oScale.ID ,10),
        pressed: parseInt(nCurrentMark) === parseInt(oScale.ID) ?  true : false,
        press: that._onButtonPress.bind(that),
        enabled: that.getEnabled()
      });
      oButton.addStyleClass("zhcm_Hap_650RatingToggleDesktop");
      oButton.data("valToSet", oScale.ID);
      that.addAggregation("_buttons", oButton);

    });
  },

  renderer: {

    render: function(oRm, oControl) {
      var bDisplayText = oControl.getDisplayText(),
        aButtons = oControl.getAggregation("_buttons"),
        bCantEstimateAvailable = oControl.getCantEstimateAvailable(),
        oScaleVals = oControl.getScale(),
        iSpacing = oControl.getSpacing();

      oRm.write("<div");
      oRm.writeControlData(oControl);
      oRm.addClass("zhcm_Hap_650MultipleToggleButtons");
      oRm.writeClasses();
      oRm.write(">");

      oRm.write("<div class='zhcm_Hap_650MarkBoxContainer'>");

      var iColumns = this._getColumnCount(aButtons.length),
        iRows = Math.ceil(aButtons.length / iColumns),
        sWidth = this._getColumnWidth(bCantEstimateAvailable ? iColumns + 1 : iColumns, iSpacing);

      for (var r = 0; r < iRows; r++) {
        for (var c = 0; c < iColumns; c++) {
          var i = r * iColumns + c,
            oButton = aButtons[i];
          if (oButton) {
            this._renderButton(oRm, oButton, sWidth, "Normal", bDisplayText ? oScaleVals[i].DESCRIPTION : null);
          } else {
            this._renderButton(oRm, null, sWidth, "Empty");
          }
        }
        if (bCantEstimateAvailable) {
          if (r === 0) {
            this._renderButton(oRm, oControl.getAggregation("_nButton"), sWidth, "NButton",
              oControl.getModel("i18n").getResourceBundle().getText("CantEval"));
          } else {
            this._renderButton(oRm, null, sWidth, "Empty");
          }
        }
      }

      oRm.write("</div>");
      oRm.write("</div>");
    },

    _renderButton: function (oRm, oButton, sWidth, sType, sText) {
      oRm.write("<div");
      oRm.addClass("zhcm_Hap_650MarkBox");
      if (sType === "NButton") {
        oRm.addClass("zhcm_Hap_650MarkBoxCE");
      }
      oRm.addStyle("width", sWidth);
      oRm.writeStyles();
      oRm.writeClasses();
      oRm.write(">");

      if (sType !== "Empty") {
        oRm.renderControl(oButton);
        if (sText) {
          oRm.write("<div class='zhcm_Hap_650ScaleValText'>" + sText + "</div>");
        }
      }

      oRm.write("</div>");
    },

    _getColumnCount: function (iButtonsCount) {
      switch (iButtonsCount) {
        case 1: case 2:
          return iButtonsCount;
        case 3: case 6: case 9:
          return 3;
        case 4: case 7: case 8: case 11: case 12:
          return 4;
        default:
          return 5;
      }
    },

    _getColumnWidth: function (iColumns, iSpacing) {
      var sDecrease = (iSpacing * iColumns / (iColumns + 1)).toFixed(2) + "px",
        sWidth = (100 / iColumns).toFixed(2) + "%";
      return "calc(" + sWidth + " - " + sDecrease + ")";
    }

  }

});

return MultipleToggleButtons;

});



// sap.ui.define([
//     "sap/ui/core/Control"
// ], function(Control) {
//     "use strict";

//     var MultipleToggleButtons = Control.extend("zhcm_Hap_650.CustomControls.MultipleToggleButtons", {

//         metadata: {

//             properties: {
//                 scale:                 {type: "Object"},
//                 displayText:           {type: "Boolean", defaultValue: true},
//                 enabled:               {type: "Boolean", defaultValue: true},
//                 cantEstimateAvailable: {type: "Boolean", defaultValue: true},
//                 cantEstimateVal:       {type: "Boolean"},
//                 value:                 {type: "string"}
//             },

//             aggregations: {
//                 _buttons: { type: "sap.m.ToggleButton", multiple: true },
//                 _nButton: { type: "sap.m.ToggleButton", multiple: false }
//             },

//         },

//         _onButtonPress: function(oEvt){
//             var oSource = oEvt.getSource(),
//                 oTopControl = oSource.getParent(),
//                 aButtons = oTopControl.getAggregation("_buttons"),
//                 oNButton = oTopControl.getAggregation("_nButton");

//             if(oNButton){
//                 aButtons.push(oNButton);
//             }

//             $.each(aButtons, function(sIndex, oButton){
//                 if(oButton.sId != oSource.sId){
//                     oButton.setPressed(false);
//                 } else {
//                     oTopControl.setValue(oButton.data().valToSet);
//                     if(oButton.data() !== "0000" && oTopControl.getCantEstimateAvailable()){
//                         oTopControl.setCantEstimateVal(false);
//                     }
//                 }
//             });

//         },

//         _onCantEstimatePress: function(oEvt){
//             var p = oEvt.getSource().getParent();
//             this._onButtonPress.apply(this, [oEvt]);
//             p.setCantEstimateVal(!p.getCantEstimateVal());
//         },

//         onBeforeRendering: function(){
//             if(this.getAggregation("_buttons")){
//                 return;
//             }

//             if(this.getCantEstimateAvailable()){
//                 var oNButton = new sap.m.ToggleButton({
//                     text: "N",
//                     pressed: this.getCantEstimateVal(),
//                     press: this._onCantEstimatePress.bind(this)
//                 });
//                 oNButton.addStyleClass("zhcm_Hap_650RatingToggleDesktop");
//                 oNButton.data("valToSet", "0000");
//                 this.setAggregation("_nButton", oNButton);
//             }

//             var oScaleVals = this.getScale(),
//                 that = this,
//                 nCurrentMark = this.getValue();

//             // this.destroyAggregation("_buttons");

//             $.each(oScaleVals, function(sIndex, oScale){
//                 var oButton = new sap.m.ToggleButton({
//                     text: parseInt(oScale.ID ,10),
//                     pressed: parseInt(nCurrentMark) == parseInt(oScale.ID) ?  true : false,
//                     press: that._onButtonPress.bind(that),
//                     enabled: that.getEnabled()
//                 });
//                 oButton.addStyleClass("zhcm_Hap_650RatingToggleDesktop");
//                 oButton.data("valToSet", oScale.ID);
//                 that.addAggregation("_buttons", oButton);

//             });
//         },

//         renderer: {

//             render: function(oRm, oControl) {
//                 oRm.write("<div");
//                 oRm.writeControlData(oControl);
//                 oRm.addClass("zhcm_Hap_650MultipleToggleButtons");
//                 oRm.writeClasses();
//                 oRm.write(">");

//                 oRm.write("<div class='zhcm_Hap_650MarkBoxContainer'>");
//                 if(oControl.getDisplayText()){
//                     this._renderWithTexts(oRm, oControl);
//                 } else {
//                     this._renderButtonsOnly(oRm, oControl);
//                 }
//                 oRm.write("</div>");

//                 if(oControl.getCantEstimateAvailable()){
//                     // oRm.write("<div class='zhcm_Hap_650ceBox'>");
//                     oRm.write("<div>");
//                     this._renderNButtonWithText(oRm, oControl);
//                     oRm.write("</div>");
//                 };

//                 oRm.write("</div>");
//             },

//             _renderButtonsOnly: function (oRm, oControl) {
//                 var aButtons = oControl.getAggregation("_buttons");
//                 $.each(aButtons, function(key, oButton){
//                     oRm.renderControl(oButton);
//                 });
//             },

//             _renderWithTexts: function (oRm, oControl) {
//                 var oScaleVals = oControl.getScale();
//                 $.each(oControl.getAggregation("_buttons"), function(key, oButton){
//                     oRm.write("<div class='zhcm_Hap_650MarkBox'>");
//                     oRm.renderControl(oButton);
//                     oRm.write("<div class='zhcm_Hap_650ScaleValText'>" + oScaleVals[key].DESCRIPTION + "</div>");
//                     oRm.write("</div>");
//                 });
//             },

//             _renderNButtonWithText: function (oRm, oControl) {
//                     var oNButton = oControl.getAggregation("_nButton");
//                     oRm.write("<div class='zhcm_Hap_650MarkBoxCE'>");
//                     oRm.renderControl(oNButton);
//                     var sCantEvalText = oControl.getModel("i18n").getResourceBundle().getText("CantEval");
//                     oRm.write("<div class='zhcm_Hap_650ScaleValText'>" + sCantEvalText + "</div>");
//                     oRm.write("</div>");
//             }
//         }

//     });

//     return MultipleToggleButtons;

// });