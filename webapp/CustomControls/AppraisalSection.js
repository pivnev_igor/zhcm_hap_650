sap.ui.define([
    "sap/ui/core/Control",
    "sap/ui/model/json/JSONModel"
], function(Control, JSON) {
    "use strict";

    var AppraisalSection = Control.extend("zhcm_Hap_650.CustomControls.AppraisalSection", {

        metadata: {

            properties: {
                container: {type: "boolean", defaultValue: false},
                inAverage: {type: "boolean", defaultValue: false},
                header: {type: "string"},
                averageVal: {type: "float"},
                scaleType: {type: "object"},
                scaleValues: {type: "object"},
                descScaleValues: {type: "object"},
                descCompScaleValues: {type: "object"},
                descTypeCtrl: { type: "string"}
            },

            aggregations: {
                content: {type: "sap.ui.core.Control", multiple: true},
                _icon: {type: "sap.ui.core.Icon", multiple: false}
            },

            defaultAggregation: "content"

        },

        init: function () {
            var oIcon = new sap.ui.core.Icon({
                src: "sap-icon://hint",
                press: this.onIconClick.bind(this)
            });
            this.setAggregation("_icon", oIcon);
        },

        renderer: {

            render: function (oRm, oControl) {

                var aContent = oControl.getContent();
                var bContainer = oControl.getContainer() || oControl.aCustomStyleClasses;
                var sText = oControl.getHeader();
                if (oControl.getInAverage()) {
                    var sAverageVal = oControl.getAverageVal() || null;
                }

                if (bContainer) {
                    oRm.write("<div");
                    oRm.writeControlData(oControl);
                    oRm.writeClasses();
                    oRm.write(">");
                }

                if (sText) {
                    oRm.write("<span");
                    oRm.addClass("zhcm_Hap_650SCHeader");
                    oRm.writeClasses();
                    oRm.write(">");
                    oRm.write(sText);
                    oRm.write("</span>");
                }

                oRm.renderControl(oControl.getAggregation("_icon"));

                if (sAverageVal) {
                    if (oControl.getScaleType().DATA_TYPE === 'M') {
                        // get color
                        var aAverageInRange = oControl.getScaleValues().filter(
                            function (oScaleVal) {
                                if (sAverageVal >= parseFloat(oScaleVal.MIN) &&
                                    sAverageVal <= parseFloat(oScaleVal.MAX)) {
                                    return true;
                                }
                            });
                        if(aAverageInRange.length > 0){
                            var sColor = aAverageInRange[0].COLOR;
                        }
                    }
                    oRm.write('<div style="display: inline-flex;justify-content: center;width:calc(100% - 200px)">');
                    oRm.write("<span style='font-size:20px;color:#CCCCCC;font-weight:700;margin:0 0 0 5rem'>");
                    oRm.write("Средняя по компетенции: ");
                    oRm.write("</span>");
                    if (sColor) {
                        var sStyle = "'font-size:20px;font-weight:700;color:" + sColor + "'";
                        oRm.write("<span style=" + sStyle + ">");
                    }
                    if (!sColor) {
                        oRm.write("<span style='font-size:20px;color:#009de0;font-weight:700'>");
                    }
                    oRm.write(sAverageVal.toFixed(1));
                    oRm.write("</span>");
                    oRm.write('</div>');
                }

                for (var i = 0; i < aContent.length; i++) {
                    oRm.renderControl(aContent[i]);
                }

                if (bContainer) {
                    oRm.write("</div>");
                }
            },

        }

    });

    AppraisalSection.prototype._getDataForPopoverM = function (oCust, aRanges) {
        var aParsedRanges = [];
        for (var i = 0; i < aRanges.length; i++) {
            aParsedRanges.push({
                RANGE_LOW: aRanges[i].MIN,
                RANGE_HIGH: aRanges[i].MAX,
                RANGE_NAME: aRanges[i].DESCRIPTION
            })
        }
        return {
            RAT_LOW: oCust.MIN,
            RAT_HIGH: oCust.MAX,
            RAT_DIST: oCust.STEP,
            RAT_TEXT: oCust.DESCRIPTION,
            Ranges: aParsedRanges || []
        };
    };
    AppraisalSection.prototype._getDataForPopoverQ = function (oCust, aRanges) {
        var aRangesMaped = [];
        for (var i = 0; i < aRanges.length; i++) {
            aRangesMaped.push({
                RATING: parseInt(aRanges[i].VALUE),
                PSTEXT: aRanges[i].DESCRIPTION
            })
        }
        return aRangesMaped;
    };
    AppraisalSection.prototype._getDataForPopoverD = function (oCust, aValues) {
        var aParsedValues = [];
        var str = '';
        for (var i = 0; i < aValues.length; i++)
        {
          if (oCust.SCALE_ID === aValues[i].SCALE_ID) {
              str = str + aValues[i].DESCRIPTION + ' ';
          }
        }
        aParsedValues.push({PSTEXT: str});
        return aParsedValues;
    };

    AppraisalSection.prototype.onIconClick = function (oEvt) {
        // 1. Create Popover type M or Q
        var oScaleCust = this.getScaleType(),
          oDescScaleValues = this.getDescScaleValues(),
          oDescCompScaleValues = this.getDescCompScaleValues(),
          oDescTypeCtrl = this.getDescTypeCtrl(),
            bIsDescScale = false,
            oDescSclVal,
            oPopoverData,
            oPopoverModel = new JSON(),
            sFragmentName = "zhcm_Hap_650.fragments.MyMarksScale";

        if(oDescTypeCtrl.toUpperCase().indexOf('COMP') + 1) {
          oDescSclVal = oDescCompScaleValues.results;
          if (oDescSclVal === null || oDescSclVal === undefined) {
            oDescSclVal = oDescCompScaleValues;
          }
        }
        else {
      oDescSclVal = oDescScaleValues.results;
      if (oDescSclVal === null || oDescSclVal === undefined) {
            oDescSclVal = oDescScaleValues;
      }
    }

        if(oDescSclVal !== null && oDescSclVal !== undefined && oDescSclVal.length > 0) {
            for (var i = 0; i < oDescSclVal.length; i++) {
                if (oScaleCust.SCALE_ID === oDescSclVal[i].SCALE_ID) {
                    bIsDescScale = true;
                    break;
                }
            }
        }

        if ( bIsDescScale === true) {
            sFragmentName += "D";
            oPopoverData = this._getDataForPopoverD(oScaleCust, oDescSclVal);
        }
        else {
          switch (oScaleCust.DATA_TYPE) {
              case 'M':
                  sFragmentName += "M";
                  oPopoverData = this._getDataForPopoverM(oScaleCust, this.getScaleValues());
                  break;
              case 'Q':
                  sFragmentName += "Q";
                  oPopoverData = this._getDataForPopoverQ(oScaleCust, this.getScaleValues());
                  break
          }
        }

        this._oMarksScalePopover = sap.ui.xmlfragment(sFragmentName, this);
        this._oMarksScalePopover.attachAfterClose(function () {
            this._oMarksScalePopover.destroy();
            this._oMarksScalePopover = null;
        }, this);

        oPopoverModel.setData(oPopoverData);
        this._oMarksScalePopover.setModel(oPopoverModel, "PopoverScale");
        if(this.getHeader() === "Оценка индикаторов"){
            this._oMarksScalePopover.setTitle("360: Оценка индикаторов");
        }
        this._oMarksScalePopover.openBy(oEvt.getSource());

    };

    return AppraisalSection;

});