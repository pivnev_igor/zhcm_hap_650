sap.ui.define([
    "sap/ui/core/Control"
], function(Control) {
    "use strict";

    var EmployeeInfo = Control.extend("zhcm_Hap_650.CustomControls.EmployeeInfo", {

        metadata: {

            properties: {
                fio:            { type: "string" },
                photo:          { type: "string" },
                pernr:          { type: "string" },
                positionText:   { type: "string" },
                orgPathText:    { type: "string" },
                enabled:        { type: "boolean", defaultValue: true }
            },

            aggregations: {
                _deleteButton: { type: "sap.m.Button", multiple: false },
            },

            events: {
                delete: { parameters: {
                        'employeeItem': {type: 'Object' }
                    }
                }
            }

        },

        init: function(){
            var that = this;
            this._oInitContext = this;
            this.setAggregation("_deleteButton", new sap.m.Button({
                text: "{i18n>Delete}",
                type: "Default",
                icon: "sap-icon://delete",
                press: function (oEvt) {
                    that.fireDelete({ 'employeeItem': oEvt.getSource().getParent() });
                },
            }));
        },

        renderer: {

            render: function (oRm, oControl) {
                var sPhoto = oControl.getPhoto() || "./res/avatar.jpg";
                oRm.write("<div class='zhcm_Hap_650EmployeeInfo'");
                oRm.writeControlData(oControl);
                oRm.write(">");

                    oRm.write("<div class='zhcm_Hap_650EmployeeInfoLeft'");

                    oRm.write("<div>");
                        oRm.write('<img class="zhcm_Hap_650UserIcon"')
                            .writeAttribute('src', sPhoto)
                            .write('/>'); // userIconDiv

                    oRm.write("</div>"); // zhcm_Hap_650EmployeeInfoLeft
                    oRm.write("<div class='zhcm_Hap_650EmployeeInfoRight'>");
                        oRm.write("<div>");

                            oRm.write("<div class='zhcm_Hap_650EInfoName'>" + oControl.getFio() + '</div>');
                            oRm.write("<div>" + oControl.getPernr() + '</div>');
                            oRm.write("<div>" + oControl.getPositionText() + '</div>');
                            oRm.write("<div>" + oControl.getOrgPathText() + '</div>');

                        oRm.write("</div>"); // zhcm_Hap_650EmployeeInfoRight

                        if (oControl.getEnabled()) {
                            var oButton = oControl.getAggregation("_deleteButton");
                            oButton.addStyleClass("zGreyButton");
                            oButton.addStyleClass("zCenteredButton");
                            oRm.write("<br>");
                            oRm.renderControl(oButton);
                        }
                    oRm.write("</div>");

                oRm.write("</div>"); // zhcm_Hap_650EmployeeInfo
            },

        }

    });

    return EmployeeInfo;

});