sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";
    return Control.extend("zhcm_Hap_650.CustomControls.DragAndDrop", {
        metadata : {
            properties: {
                indicatorsScale:       {type : "Object"},
                leftColumnSettings:    {type : "Object"},
                middleColumnSettings:  {type : "Object"},
                rightColumnSettings:   {type : "Object"},
                cantEstimateAvailable: {type: "Boolean"},
            },
            aggregations: {
                items:     {type: "zhcm_Hap_650.CustomControls.IndicatorTile", multiple: true},
                _noValue:  {type: "zhcm_Hap_650.CustomControls.IndicatorTile", multiple: true},
                _rightCol: {type: "zhcm_Hap_650.CustomControls.IndicatorTile", multiple: true},
                _leftCol:  {type: "zhcm_Hap_650.CustomControls.IndicatorTile", multiple: true}
            },
            events: {}
        },

        init : function () {
        },

        ondrop: function(oEvent){
            var sTileId = oEvent.originalEvent.dataTransfer.getData("text");
            var oTile = document.getElementById(sTileId);
            var sIndicatorGroup;

            if(sTileId.indexOf("tile") === -1){
                return;
            }

                function setRated(oTile, sRating) {
                    var items = this.getAggregation("items");
                    for(var i=0; i<items.length; i++){
                        if(items[i].getId() === oTile.id){
                            items[i].setProperty("rated", sRating);
                            if(this.getCantEstimateAvailable()){
                                var bCantEstimate = sRating === "0000" ? true : false;
                                items[i].setProperty("cantEstimate", bCantEstimate);
                            }
                            return;
                        }
                    }
                };

            if(oEvent.target.className.indexOf("zhcm_Hap_650IndicatorContainer") > -1) {
                sIndicatorGroup = oEvent.target;
            } else if (oEvent.target.parentElement.className.indexOf("zhcm_Hap_650IndicatorContainer") > -1) {
                sIndicatorGroup = oEvent.target.parentElement;
            };

            sIndicatorGroup.appendChild(oTile);
            switch (sIndicatorGroup.id) {
                case "_rightCol" :
                    setRated.call(this, oTile, this.getRightColumnSettings().ID);
                    break;
                case "_leftCol":
                    setRated.call(this, oTile, this.getLeftColumnSettings().ID);
                    break;
                case "_noValue":
                    setRated.call(this, oTile, this.getMiddleColumnSettings().ID);
                    break;
                default:
            }
        },

        ondragstart: function(oEvent){
            oEvent.originalEvent.dataTransfer.setData("text", oEvent.target.id);
        },
        ondragover: function(oEvent){
            oEvent.preventDefault();
        },

        renderer : function (oRM, oControl) {
            var renderIndicators = function (aIndicators, oRM, sRated){
                aIndicators = aIndicators || [];
                for(var i=0; i<aIndicators.length; i++){
                    if (aIndicators[i].getProperty("rated") === sRated){
                        oRM.renderControl(aIndicators[i]);
                    }
                }
            };

            var renderIndicatorContainer = function(oRM, oControl, sId, sRatingToSet, sTitle){
                oRM.write('<div id=' + sId + ' draggable=false');
                oRM.addClass("zhcm_Hap_650IndicatorContainer");
                switch (sId) {
                    case "_rightCol":
                        oRM.addClass("zhcm_Hap_650IndicatorContainerOk");
                        break;
                    case "_leftCol":
                        oRM.addClass("zhcm_Hap_650IndicatorContainerNotOk");
                        break;
                }
                oRM.writeClasses();
                oRM.write(">");
                oRM.write("<div style='text-align:center'>" + sTitle +
                    "<hr style='border-top: 1px dotted #8c8b8b'>" +
                    "</div>");

                renderIndicators(oControl.getAggregation("items"), oRM, sRatingToSet);

                oRM.write("</div>");
            };

            oRM.write("<div");
            oRM.writeControlData(oControl);
            oRM.addClass("zhcm_Hap_650IndicatorsDragAndDrop");
            oRM.writeClasses();
            oRM.write(">");

            var oLeftCol = oControl.getLeftColumnSettings();
            var oMidCol = oControl.getMiddleColumnSettings();
            var oRightCol = oControl.getRightColumnSettings();
            var sIndTitle = oControl.getModel("i18n").getResourceBundle().getText("Indicators");

            renderIndicatorContainer(oRM, oControl, "_leftCol", oLeftCol.ID, oLeftCol.DESCRIPTION);
            renderIndicatorContainer(oRM, oControl, "_noValue", oMidCol.ID, sIndTitle);
            renderIndicatorContainer(oRM, oControl, "_rightCol", oRightCol.ID, oRightCol.DESCRIPTION);

            oRM.write("</div>");
        },

    });
});