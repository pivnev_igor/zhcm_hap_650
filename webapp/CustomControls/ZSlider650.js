sap.ui.define([
    "jquery.sap.global",
    "sap/ui/core/Control",
    "sap/m/Slider",
    "sap/m/Input",
    "sap/ui/core/EnabledPropagator"
], function ($, Control, Slider, Input, EnabledPropagator) {
    "use strict";

    var ZSlider650 = Control.extend("zhcm_Hap_650.CustomControls.ZSlider650", {

        metadata: {
            properties: {
                scale:   {type: "Object"},
                width:   {type: "sap.ui.core.CSSSize", group: "Appearance", defaultValue: "100%"},
                name:    {type: "string", group: "Misc", defaultValue: ""},
                min:     {type: "float", defaultValue: 0},
                max:     {type: "float", defaultValue: 100},
                step:    {type: "float", defaultValue: 1},
                value:   {type: "float", defaultValue: 0},
                textValue:  {type: "string", defaultValue: true},
                enabled: {type: "boolean", defaultValue: true}
            },
            aggregations: {
                _slider: {type: "sap.m.Slider", multiple: false},
                _input:  {type: "sap.m.Input", multiple: false}
            },
            events: {
                /**
                 * This event is triggered if average competency mark has to be calculated.
                 */
                calcAverage: {
                    parameters: {
                        sliderWrapper: {type: "Object"}
                    }
                }
            }
        },

        CONST: {
          trackNamePart: '-zslider650-track'
        },

        constructor: function (mSettings) {
            Control.apply(this, arguments);

            this._bSetValueFirstCall = true;

            if (mSettings.scale) {
                var oSlider = new Slider({
                    min: mSettings.min,
                    max: mSettings.max,
                    step: mSettings.step,
                    width: mSettings.width,
                    value: mSettings.value,
                    enableTickmarks: true,
                    progress: false,
                    showAdvancedTooltip: true,
                    showHandleTooltip: false,
                    enabled: mSettings.enabled,
                    liveChange: this._onRate.bind(this),
                    change: this._onChange.bind(this)
                });
                this.setAggregation("_slider", oSlider);

                // debugger;
                var oInput = new Input({
                    value: mSettings.textValue,
                    _value: mSettings.value,
                    type: sap.m.InputType.Number,
                    enabled: mSettings.enabled,
                    change: this._onInputChange.bind(this)
                }).addStyleClass("zSliderInput");
                this.setAggregation("_input", oInput);
            }
        },

        renderer: {
            render: function (oRM, oControl) {
                var ctrlName = oControl.getName() !== "" ? oControl.getName() : oControl.createGuid();
                var ctrl = oControl.getAggregation("_slider");
                ctrl.setProperty("name", ctrlName, true);

                oRM.write("<div");
                oRM.writeControlData(oControl);
                oRM.addClass("sapUiMediumMarginBottom");
                oRM.addClass("zslider650");
                oRM.writeClasses();
                oRM.write(">");
                oRM.renderControl(oControl.getAggregation("_slider"));
                oRM.renderControl(oControl.getAggregation("_input"));
                oRM.write("</div>");
            }
        },

        getMin: function () {
            return this.getAggregation("_slider").getProperty("min");
        },
        getMax: function () {
            return this.getAggregation("_slider").getProperty("max");
        },
        getStep: function () {
            return this.getAggregation("_slider").getProperty("step");
        },
        getValue: function () {
            return this.getAggregation("_slider").getProperty("value");
        },
        setValue: function (value) {
            value = typeof value === "string" ? parseFloat(value) : value;
            this.getAggregation("_slider").setValue(value);
            this.getAggregation("_input").setValue(value);
        },
        setMin: function (min) {
            this.getAggregation("_slider").setMin(min);
        },
        setMax: function (max) {
            this.getAggregation("_slider").setMax(max);
        },
        setStep: function (step) {
            this.getAggregation("_slider").setStep(step);
        },

        onAfterRendering: function () {
            var oScaleVals = this.getScale(),
                that = this;

            this._changeDeafultStyles();
            this._addColorIfNotSet(oScaleVals);

            $.each(oScaleVals, function (sIndex, oScale) {
                var nTrackPartWidth = (((oScale.MAX - oScale.MIN) / (that.getMax() - that.getMin())) * 100).toFixed(1);
                var text = oScale.DESCRIPTION.substring(0, 32) + " ...<br/>(" + oScale.MIN + " - " + oScale.MAX + ")";

                var elem = $("<div>", {
                    id: that.getName() + "-zslider650-part" + sIndex,
                    class: "zslider650-part",
                    style: "background-color: " + oScale.COLOR + "; width: " + nTrackPartWidth + "%;",
                    html: $("<span>", {
                        id: that.getName() + "-zslider650-part-text-" + sIndex,
                        class: "zslider650-part-text",
                        html: text
                    })
                });

                that.$().find(".zslider650-track").append(elem);
            });

            this.$().find(".zslider650-part-text")
                .on("mousedown", this._onClickPartText.bind(this));

            this._onRate();
        },

        _addColorIfNotSet: function(aScaleParts){
            $.each(aScaleParts, function (sIndex, oScale) {
                if (!this._isColor(oScale.COLOR)) {
                    oScale.COLOR = "lightgray";
                }
            }.bind(this));
        },

        _changeDeafultStyles: function () {
            var sTrackId = this.getName() + this.CONST.trackNamePart;

            this.$().find(".sapMSliderInner")
                .css("background-color", "transparent");
            this.$().find(".sapMSliderHandle")
                .css("background-color", "white")
                .css("box-shadow", "2px 2px 8px rgba(0,0,0,0.5)");
            this.$().find(".sapMSliderLabel")
                .css("color", "transparent");


            this.$().find(".sapMSliderTickmarks")
                .before('<div id="' + sTrackId + '" class="zslider650-track"></div>');

        },

        _onClickPartText: function (event) {
            event.stopImmediatePropagation();
            var actionCtrl = event.target;  // event.eventPhase = 2 !!!
            var arr = actionCtrl.id.split("-");
            var sIndex = arr[arr.length - 1];

            var part = this.getScale()[sIndex];
            var str = part.DESCRIPTION + ", (" + part.MIN + " - " + part.MAX + ")";
            var oData = {"DESCRIPTION": str};

            var oModel = new sap.ui.model.json.JSONModel();
            if (!this._oPopover) {
                this._oPopover = sap.ui.xmlfragment("zhcm_Hap_650.fragments.ZSlider650", this);

                oModel.setData(oData);

                this._oText = new sap.m.Text({    // .bindText("{textShort}");
                    text: "{/DESCRIPTION}"
                }).addStyleClass("zslider650-active-part-tooltip");

                this._oText.setModel(oModel);
                this._oText.placeAt(this._oPopover);

                this._oPopover.addContent(this._oText);
            } else {
                oModel = this._oText.getModel();
                oModel.setData(oData);
                this._oText.setModel(oModel);
            }

            this._oPopover.openBy(actionCtrl);
        },

        createGuid: function () {
            return "xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0,
                    v = c === "x" ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        },

        onExit: function () {
            if (this._oPopover) {
                this._oPopover.destroy();
            }
        },

        _onChange: function (oEvt) {
            this._bSetValueFirstCall = true;
            this._onRate(oEvt);
            this.fireCalcAverage({sliderWrapper: oEvt.getSource().getParent()});
        },

        _onInputChange: function (oEvt) {
            var fNewValue = Number(oEvt.getParameter("newValue"));
            this.getAggregation("_slider").setValue(fNewValue);
            this._onChange(oEvt);
        },

        _onRate: function (oEvt) {
            var nIndex = null;
            var value = this.getAggregation("_slider").getValue();
            var parts = this.$().find(".zslider650-track > div");
            var part = null;
            var oScaleVals = this.getScale();

            if (oEvt) {
                this._bSetValueFirstCall = false;
            }

            if (this._bSetValueFirstCall === false) {
                var oInput = this.getAggregation("_input");
                if (Number(oInput.getValue()) !== value) {
                    oInput.setValue(value);
                }
            }

            $.each(oScaleVals, function (sIndex, oScale) {
                if ((part === null) && (value <= oScale.MAX)) {
                    nIndex = sIndex;
                    part = parts[sIndex];
                }
            });

            var prevPart = this.$().find(".zslider650-track .zslider650-active-part");
            if (prevPart) {
                $(prevPart).removeClass("zslider650-active-part");
            }

            if (part) {
                $(part).addClass("zslider650-active-part");
            }

            if (nIndex) {
                var bkcolor = oScaleVals[nIndex].COLOR;
                this.$().find(".sapMSliderHandleTooltip")
                    .css("color", bkcolor)
                    .css("font-weight", "bold");

                this.$().find(".zSliderInput > input")
                    .css("color", bkcolor);
            }
        },

        _isColor: function (sColor) {

            if (!sColor || sColor.length !== 7) {
                return false;
            }

            var s = new Option().style;
            s.color = sColor;

            var test1 = s.color === sColor;
            var test2 = /^#[0-9A-F]{6}$/i.test(sColor);

            return test1 || test2 ? true : false;
        }


    });

    EnabledPropagator.call(ZSlider650.prototype);

    return ZSlider650;
});