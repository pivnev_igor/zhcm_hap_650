sap.ui.define([
    "sap/ui/core/util/MockServer",
    "zhcm_Hap_650/Utils/Config"
], function(MockServer, Config) {
    "use strict";

    return {

        init: function() {
            var oMockServer = new MockServer({
                rootUri: Config.PM_360_SERVICE
            });

            oMockServer.simulate("../webapp/localService/metadata.xml", {
                sMockdataBaseUrl: "../webapp/localService/mockdata",
                bGenerateMissingMockData: true
            });

            oMockServer.start();

        }

    };

});