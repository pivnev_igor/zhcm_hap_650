sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "sap/m/MessageBox",
        "sap/ui/model/Filter",
        "../Utils/Config",
        "zhcm_Hap_650/Utils/DataAccess",
        "../Utils/Formatter"
    ],
    function (Controller, JSONModel, MessageBox, Filter, Config, DataAccess, Formatter) {
        return Controller.extend("zhcm_Hap_650.Controller.FormReportDialog", {
          formatter: Formatter,
      // Constants
      REPORTKEY_CONS    : "ZHCM_PM_XXXX_REP_CONS",
      REPORTKEY_FINSTAT : "ZHCM_PM_0661_FINSTAT",
      REPORTKEY_IND     : "ZHCM_PM_XXXX_REP_IND",

            _oDialog: null,
            _helpDialog: null,
            _oModelDialog: null,
            _oHelpList: null,
            _oModelFormat: null,

            _oFormatConfig: {
              "Result" :  [
                {"KEY" : "PDF" , "DESC" : 'PDF' },
                {"KEY" : "EXCEL" , "DESC" : 'Excel' }
              ]
            },

            filterInputId: null,
            configFilterName : {
              // фИО
              "ReportFIO": ["FIO", sap.ui.model.FilterOperator.Contains],
              // Процедура оценки
              "ReportProc": ["ID_EVAL_PROC", sap.ui.model.FilterOperator.EQ],
              // Организационная единица
              "ReportProcOrg": ["EVAL_PROC_ORG", sap.ui.model.FilterOperator.EQ]
          },

            sPernr: "",

            onInit: function () {
              var that = this;
              this._oDialog = this.getView().byId('reportDialogView');
              this._oModelDialog = this._oDialog.getModel("ModelDialog");

              this._oFormatModel = new JSONModel(this._oFormatConfig);
              this._oDialog.setModel(this._oFormatModel, "FormatModel");


              var d = DataAccess.getUserMemId();

              d.then(function(aPernr){
                  that.sPernr = aPernr[0].PERNR_MEMID;
                });

            },

            onSendReport: function (oEvent) {

                // Send Report $value
                var oModelDialog = this._oDialog.getModel("ModelDialog"),
                    sReportKey = oModelDialog.getProperty("/REPORTKEY"),
                    sPernr     = oModelDialog.getProperty("/PERNR") || "00000000",
                    sIdEvalProc = oModelDialog.getProperty("/ID_EVAL_PROC"),
                    sEvalProcOrg = oModelDialog.getProperty("/EVAL_PROC_ORG"),
                    sFormatKey = "",
                    oFormatItem = this.getView().byId('ReportFormat').getSelectedItem();

                if (oFormatItem) {
                  sFormatKey = oFormatItem.getKey();
                }

                this.getView().byId("ReportProc").setValueState(sap.ui.core.ValueState.None);
                this.getView().byId("ReportFIO").setValueState(sap.ui.core.ValueState.None);

                if(this._checkObligatoryAndSetState(sReportKey, sIdEvalProc, sPernr, sEvalProcOrg) === false){
                    MessageBox.error("Заполните обязательные поля");
                    return;
                }

                this._oDialog.close();

                var that = this;

                if (sReportKey === that.REPORTKEY_IND) {
                  DataAccess.getReportCheck(sPernr, sIdEvalProc, sFormatKey, sReportKey).then(
                      function(oObj) {
                          if (oObj !== null && oObj !== undefined && oObj.FLAG === true) {
                              var sUrlParams = "REPORTKEY='" + sReportKey +
                                  "',PERNR='" + sPernr +
                                  "',ID_EVAL_PROC='" + sIdEvalProc +
                                  "',PERNRS_STRING='" + "" + // нужен только для консолидированного отчета
                                  "',FORMAT='" + sFormatKey +
                                  "'";
                              var sPath = window.location.origin + Config.PM_360_SERVICE +
                                  "ReportSet(" + sUrlParams + ")/$value";

                              that._openWindowPrint(sPath);
                          } else {
                            MessageBox.show("Оценка ещё не завершена", {
                                icon: MessageBox.Icon.INFORMATION,
                                title: "Информация",
                                actions: [MessageBox.Action.OK]
                            });
                          }
                          that._clearFields();
                      },
                      function(oError) {
                          console.log("Error: " + oError);
                          that._clearFields();
                          MessageBox.show("Оценка ещё не завершена", {
                              icon: MessageBox.Icon.INFORMATION,
                              title: "Информация",
                              actions: [MessageBox.Action.OK]
                          });
                      }
                    );
                } else {
                    var sUrlParams = "REPORTKEY='" + sReportKey +
                                "',PERNR='" + sPernr +
                                "',ID_EVAL_PROC='" + sIdEvalProc +
                                "',PERNRS_STRING='" + "" + // нужен только для консолидированного отчета
                                "',FORMAT='" + sFormatKey +
                                "'";
                    var sPath = window.location.origin + Config.PM_360_SERVICE +
                        "ReportSet(" + sUrlParams + ")/$value";

                    that._openWindowPrint(sPath);
                }
            },

            _checkObligatoryAndSetState: function(sReportKey, sIdEvalProc, sPernr, sEvalProcOrg){
                var bCheckSuccessful = true;

                if (!sIdEvalProc && !sEvalProcOrg) {
                    this.getView().byId("ReportProc").setValueState(sap.ui.core.ValueState.Error);
                    this.getView().byId("ReportProcOrg").setValueState(sap.ui.core.ValueState.Error);
                    bCheckSuccessful = false;   
                }                
                
                if (sReportKey !== this.REPORTKEY_FINSTAT && !parseInt(sPernr)) {
                    this.getView().byId("ReportFIO").setValueState(sap.ui.core.ValueState.Error);
                    bCheckSuccessful = false;
                }

                return bCheckSuccessful;
            },

            onSendReject: function () {
              this._oDialog.close();
              this._clearFields();
            },

            onValueHelpReportFIO: function (oEvent) {
                var that = this,
                    aFilters = [],
                    aDataAccessFilter = [],
                    sEntity = '';

                this._oModelDialog = this._oDialog.getModel("ModelDialog");

                var sFIO = this._oModelDialog.getProperty("/FIO");
                var sIdEvalProc = this._oModelDialog.getProperty("/ID_EVAL_PROC");

                this.filterInputId = this._getFilterId(oEvent);

                this._setBusy(true);

                aFilters.push(new Filter({
                        path: 'FIO',
                        operator: 'EQ',
                        value1: sFIO
                    })
                );

                aDataAccessFilter.push(new Filter({
                        path: 'ID_EVAL_PROC',
                        operator: 'EQ',
                        value1: sIdEvalProc
                    })
                );

                this._oDialog.getModel("ModelDialog").getProperty("/REPORTKEY") === this.REPORTKEY_IND
                    ? sEntity = "ReportFIOApre"
                    : "ReportFIO";

                DataAccess.getReportFIO(aDataAccessFilter, sEntity)
                    .then(function (oObj) {
                        var oModel = new JSONModel({});
                        oObj.REPORTKEY = that._oDialog.getModel("ModelDialog").getProperty("/REPORTKEY");
                        oModel.setData(oObj);
                        that._setBusy(false);
                        that._helpDialogOpen("DialogFIO", {url: "ReportFIOSet"}, aFilters, oModel);
                    });
            },

            onValueHelpReportProc: function (oEvent) {
              var that = this;
              var aFilters = [];
              this._oModelDialog = this._oDialog.getModel("ModelDialog");
                var sProcId = this._oModelDialog.getProperty("/ID_EVAL_PROC");

                this.filterInputId = this._getFilterId(oEvent);

                aFilters.push(
                    new Filter({
                       path: 'PERNR',
                         operator: 'EQ',
                         value1: that.sPernr
                    })
                );

                var oModel = new JSONModel({});
                this._setBusy(true);
                DataAccess.getReportProc(aFilters).then(
                function(oObj){
                    oModel.setData(oObj);
                    that._setBusy(false);
                    that._helpDialogOpen("DialogProc", {url : "ReportProcSet"}, aFilters, oModel);
                });
            },

            onValueHelpReportProcOrg: function (oEvent) {
              var that = this,
                  aFilters = [];

              this._oModelDialog = this._oDialog.getModel("ModelDialog");

              var sProcId = this._oModelDialog.getProperty("/ID_EVAL_PROC");

              this.filterInputId = this._getFilterId(oEvent);

              aFilters.push(
                  new Filter({
                      path: 'ID_EVAL_PROC',
                        operator: 'EQ',
                        value1: sProcId
                  })
              );

              var oModel = new JSONModel({});
              this._setBusy(true);
              DataAccess.getReportProcOrg(aFilters).then(
                  function(oObj){
                      oModel.setData(oObj);
                      that._setBusy(false);
                      that._helpDialogOpen("DialogProcOrg", {url : "ReportProcOrgSet"}, aFilters, oModel);
                  }
              );
            },

            handleHelpClose: function (oEvent) {
                this._helpDialog.destroy(true);
            },

            _openWindowPrint: function (sPath, result) {
              var newWindow;

                newWindow = window.open(sPath);

              $(newWindow).load(function () {
                  this.print();
              });
          },

            _helpDialogOpen: function (sDialogName, oURLParam, aFilter, oModel) {
              var fragmentName = "zhcm_Hap_650.fragments." + sDialogName;

              this._helpDialog = sap.ui.xmlfragment(fragmentName, this);
              this.getView().addDependent(this._helpDialog);
              this._helpDialog.setModel(oModel, "HelpModel");

              this._helpDialog._getCancelButton().setType("Reject");

              var i18nModel = new sap.ui.model.resource.ResourceModel({
                    bundleUrl : "i18n/i18n.properties"
                });
              this._helpDialog.setModel(i18nModel, "i18n");

              this._helpDialog.open();
          },

          _handleValueHelpSearch: function (oEvent) {
              var sValue = oEvent.getParameter("value");
              var sFilterParam = this.configFilterName[this.filterInputId][0];
              if(sFilterParam === "EVAL_PROC_ORG") {
                  sFilterParam = "PROC_ORG_STEXT";
              }
              var oFilter = new Filter(sFilterParam, sap.ui.model.FilterOperator.Contains, sValue);
              var oBinding = oEvent.getSource().getBinding("items");
              oBinding.filter([oFilter]);
          },

          _handleValueHelpClose: function (oEvent) {
              var that = this;
              this._oModelDialog = this._oDialog.getModel("ModelDialog");
              var aContexts = oEvent.getParameter("selectedContexts");
              var sFilterParam = this.configFilterName[this.filterInputId][0];

              this.getView().byId("ReportProc").setValueState(sap.ui.core.ValueState.None);
              this.getView().byId("ReportFIO").setValueState(sap.ui.core.ValueState.None);

              if (aContexts !== undefined && aContexts.length) {
                  aContexts.map(function (oContext) {
                        var oObject = oContext.getObject();
                        that._oModelDialog.setProperty("/" + sFilterParam , oObject[sFilterParam]);
                        if(that.filterInputId === "ReportFIO") {
                            that._oModelDialog.setProperty("/PERNR", oObject["PERNR"]);
                        }
                        if(that.filterInputId === "ReportProcOrg") {
                            that._oModelDialog.setProperty("/PROC_ORG_STEXT", oObject["PROC_ORG_STEXT"]);
                            that._oModelDialog.setProperty("/NAME_EVAL_PROC", oObject["NAME_EVAL_PROC"]);
                        }
                    });
                  oEvent.getSource().getBinding("items").filter([]);
              }
          },

          _getFilterId: function (oEvent) {
              var viewId = oEvent.getSource().getId();
              var aIds = viewId.split("--");
              return aIds.pop();
          },

          _getText: function(sMark) {
            var rB = this.getOwnerComponent().getModel("i18n").getResourceBundle();
            return rB.getText(sMark);
          },

          _clearFields: function(){
                var modelDialog =	this._oDialog.getModel("ModelDialog");
            modelDialog.setProperty( "/FIO", "" );
            modelDialog.setProperty( "/ID_EVAL_PROC", "" );
            modelDialog.setProperty( "/PERNR", "" );
          },

          _setBusy: function(bVal){
                this.getView().byId('reportDialogView').setBusy(bVal);
            }


        });
    });