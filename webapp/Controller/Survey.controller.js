sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "sap/ui/core/mvc/Controller",
    "../Utils/Config",
    "../Utils/DataAccess",
    "../Utils/Formatter",
    "sap/m/MessageBox",
    "sap/m/Popover",
    "sap/m/MessagePopover",
    "sap/m/MessagePopoverItem",
    "sap/ui/model/CompositeType",
    "sap/m/Text",
    "sap/m/TextRenderer"
], function (JSONModel, Fragment, Controller, Config, DataAccess, Formatter, MessageBox, Popover, MessagePopover, MessagePopoverItem, CompositeType, Text, TextRenderer) {
    "use strict";

    var ZClickableText = Text.extend("zhcm_IPR.CustomControls.ZClickableText", {
        metadata: { events: { press: {} } },
        ontap: function () { this.firePress({}); }
    });
    zhcm_IPR.CustomControls.ZClickableTextRenderer = TextRenderer;
    
    // Значение слайдера
    CompositeType.extend("zhcm_IPR.Utils.types.ZSliderValue", {

        constructor : function () {
            CompositeType.apply(this, arguments);
            this.sName = "ZSliderValue";
            // this.bUseRawValues = true;
        },
    
        parseValue: function (iValue, sType) {
            return [iValue, false];/*[M_VALUE, IS_UNSET]*/
        },
    
        formatValue: function (aValues/*[M_VALUE, IS_UNSET]*/, sType) {
            var iValue = aValues[0];
            return iValue;
        },
    
        validateValue: function () {}
    });

    // Значение инпута слайдера
    CompositeType.extend("zhcm_IPR.Utils.types.ZSliderTextValue", {

        constructor : function () {
            CompositeType.apply(this, arguments);
            this.sName = "ZSliderTextValue";
            // this.bUseRawValues = true;
        },
    
        parseValue: function (iValue, sType) {
            return [iValue, false];/*[M_VALUE, IS_UNSET]*/
        },
    
        formatValue: function (aValues/*[M_VALUE, IS_UNSET]*/, sType) {
            var iValue = aValues[0];
            return iValue;
        },
    
        validateValue: function () {}
    });

    var oMessagePopover = new MessagePopover({
        items: {
            path: "/",
            template: new MessagePopoverItem({
                type: "{type}",
                title: "{title}"
            })
        }
    });

    return Controller.extend("zhcm_Hap_650.Controller.Survey", {
        formatter: Formatter,

        onInit: function () {
            // #DEBUG START
            // if (window.location.hostname.toLowerCase() === "sms00990" ||
            //     window.location.hostname === "127.0.0.1" ||
            //     window.location.hostname.toLowerCase() === "localhost") window.goControllerSu = this;
            // #DEBUG END

            this.getOwnerComponent().getRouter().getRoute("survey").attachMatched(this._onRouteMatched, this);

            var oViewModel = new JSONModel({
                layout: "OneColumn",
                HeaderExpanded: true
            });

            this.getView().setModel(oViewModel, "viewData");

            var oSurveyModel = new JSONModel();
            this.oView.setModel(oSurveyModel, "SurveyData");

            var oMessagesModel = new JSONModel();
            oMessagePopover.setModel(oMessagesModel);

            var oEventBus = sap.ui.getCore().getEventBus();
            oEventBus.subscribe(
                "isCompRatingCompleted",    //канал
                "iconComplete",             //название события
                this._onIconComplete,       //вызываем метод _onIconComplete в данном (this) контроллере
                this
            );

        },

        //метод, который будет вызываться, при отправке события из компонента
        _onIconComplete: function (channel, event, item) {
            console.log("<***> channel=", channel, ", event=", event, ", item=", item);
            if (item) {                
                var selector = '[data-compid="' + item.COMP_ID + '"]',
                    oCompUICtrl = $(selector);

                if (oCompUICtrl) {
                    selector = "#" + oCompUICtrl.attr("Id");
                    var bFlag = $(selector).hasClass("zCompWithError");
                    if (bFlag) {
                        var aCompListItems = this.getView().byId("competenciesList").getItems();
                        if (aCompListItems) {
                            for (var i = 0; i < aCompListItems.length; i++) {
                                var sCompId = aCompListItems[i].data().compId;
                                if (item.COMP_ID === sCompId) {
                                    aCompListItems[i].removeStyleClass("zCompWithError");
                                    break;
                                }
                            }
                        } 
                    }                   
                }                               
            }
        },

        _onRouteMatched: function (oEvt) {
            var oArgs = oEvt.getParameter("arguments"),
                that = this,
                fnResponseParser = this._parseSurveyData.bind(this),
                sPenrMemId = this._getAppData("/CurrentPernrMemId");

            this.getView().setBusy(true);
            this._cleanUp();

            if (sPenrMemId) {
                DataAccess.getSurveyData(oArgs.AppraisalId, sPenrMemId, fnResponseParser);
            } else {
                DataAccess.getUserMemId()
                    .then(
                        function (oMemIds) {
                            return DataAccess.getSurveyData(oArgs.AppraisalId, oMemIds[0].PERNR_MEMID, fnResponseParser);
                        },
                        function (oError) {
                            that.oView.setBusy(false);
                        })
            }
        },

        // accepts any amount of boolean arguments, if any is false - sets false
        _setViewAvailability: function () {
            // var bActionsEnabled = Array.prototype.every.call(arguments, function (b) { return b; });
            var bActionsEnabled = !!Math.min.apply(null, arguments);
            
            this.getView().getModel("viewData").setProperty("/ViewActionsEnabled", bActionsEnabled);
        },

        _isViewAvailableByStatus: function (sTaskStatus) {
            return !Config.taskStatusClosed.indexOf(sTaskStatus) > -1;
        },

        _getAppData: function (sProperty) {
            return this.getOwnerComponent().getModel("AppData").getProperty(sProperty);
        },
        _setAppData: function (sPropName, Value) {
            this.getOwnerComponent().getModel("AppData").setProperty(sPropName, Value);
        },

        _parseSurveyData: function (oData, bOpenFirstItem) {
            var headerData = $.extend({}, oData),
                that = this,
                oViewData = this.getView().getModel("viewData");
            this.getView().setModel(new JSONModel(headerData), "Person");

            if (oData.LOCKED) sap.m.MessageBox.warning(oData.LOC_MSG);
            var sHdrInstructionText = DataAccess.getInstrText.call(this, 'SU', 'HEADER', oData.ID_EVAL_PROC);
            var sInstructionText = DataAccess.getInstrText.call(this, 'SU', 'BTN_POPUP', oData.ID_EVAL_PROC);

            oViewData.setProperty("/AddInfoText", sHdrInstructionText);
            oViewData.setProperty("/InstructionButton", sInstructionText);
            oViewData.setProperty("/IdEvalProc", oData.ID_EVAL_PROC);
            this._setAppData("/NameEvalProc", oData.NAME_EVAL_PROC);
            this._setAppData("/TaskTxt", oData.TASK_TXT);
            this._setViewAvailability(this._isViewAvailableByStatus(oData.INDICATOR_ID));

            var mSurveyData = oData;
            mSurveyData.Competencies = oData.Competencies.results || oData.Competencies;
            $.each(mSurveyData.Competencies, function (iInd, Competency) {
                
                Competency.MARK = ""; // TODO: REMOVE!

                Competency._MARK_FLOAT = parseFloat(Competency.MARK) || 0;
                Competency.Indicators = Competency.Indicators.results || Competency.Indicators;
                $.each(Competency.Indicators, function (iInd, Indic) {
                    // Indic.M_VALUE = "";
                    Indic.M_VALUE = parseFloat(Indic.M_VALUE);
                    if (!Indic.M_VALUE) Indic.Z_UNSET = true;
                });
                if (mSurveyData.IN_AVERAGE) {
                    Competency._AverageVal = that._calcAverage(Competency, mSurveyData.IndicatorScale.DATA_TYPE);
                }
            });

            mSurveyData.CompetencyScaleValues = oData.CompetencyScaleValues.results || oData.CompetencyScaleValues;
            mSurveyData.IndicatorScaleValues = oData.IndicatorScaleValues.results || oData.IndicatorScaleValues;
            mSurveyData.DescriptionScaleValues = oData.DescriptionScaleValues.results || oData.DescriptionScaleValues;
            mSurveyData.DescriptionCompScaleValues = oData.DescriptionCompScaleValues.results || oData.DescriptionCompScaleValues;
            mSurveyData.CompetencyScale.MIN = parseFloat(mSurveyData.CompetencyScale.MIN);
            mSurveyData.CompetencyScale.MAX = parseFloat(mSurveyData.CompetencyScale.MAX);
            mSurveyData.CompetencyScale.STEP = parseFloat(mSurveyData.CompetencyScale.STEP);
            mSurveyData.IndicatorScale.MIN = parseFloat(mSurveyData.IndicatorScale.MIN);
            mSurveyData.IndicatorScale.MAX = parseFloat(mSurveyData.IndicatorScale.MAX);
            mSurveyData.IndicatorScale.STEP = parseFloat(mSurveyData.IndicatorScale.STEP);

            this.getView().getModel("SurveyData").setData(mSurveyData);

            if (bOpenFirstItem) {
                var oList = this.getView().byId("competenciesList");
                var firstItem = oList.getItems()[0];
                if (firstItem) {
                    firstItem.firePress();
                    oList.setSelectedItem(firstItem);
                }
            }

            this.getView().setBusy(false);
        },

        onSliderCalcAverage: function (oEvt) {
            var s = oEvt.getParameter("sliderWrapper"), // slider
                sIndicPath = s.getBindingInfo("scale").binding.getContext().getPath(),
                sCompPath = sIndicPath.split("Indicators")[0],
                oComp = this.getView().getModel("SurveyData").getProperty(sCompPath),
                sAvrgPath = sCompPath + "/_AverageVal";

            this.getView().getModel("SurveyData").setProperty(sAvrgPath, this._calcAverage(oComp));
        },

        _calcAverage: function (oComp, sScaleDataType) {
            var iIndicatorsWithRating = 0,
                fCompRating = 0;

            sScaleDataType = sScaleDataType
                || this.getView().getModel("SurveyData").getProperty("/IndicatorScale/DATA_TYPE");

            $.each(oComp.Indicators, function (sInd, oIndic) {
                if (oIndic.CANT_ESTIMATE === true) {
                    return;
                }
                if (sScaleDataType === 'Q' && oIndic.Q_VALUE) {
                    fCompRating += parseInt(oIndic.Q_VALUE);
                    iIndicatorsWithRating += 1;
                }
                if (sScaleDataType === 'M') {
                    fCompRating += Number(oIndic.M_VALUE);
                    iIndicatorsWithRating += 1;
                }
            });

            return iIndicatorsWithRating ? fCompRating / iIndicatorsWithRating : 0;
        },

        onItemPress: function (oEvt) {
            var oCompContext = oEvt.getParameter("listItem").getBindingContext("SurveyData");
            this._composeUIForCompetencyAppraisal(oCompContext);
            // oPanel.bindAggregation("content", sPathDesktop, oFragment); going to use aggr binding for mobile
            this._triggerCompIconFormatter();

        },

        onCompetencePress: function (oEvt) {
            var oCompContext = oEvt.getSource().getBindingContext("SurveyData");
            this._composeUIForCompetencyAppraisal(oCompContext);
            // oPanel.bindAggregation("content", sPathDesktop, oFragment); going to use aggr binding for mobile
            //this.getView().getModel("viewData").setProperty("/layout", sap.f.LayoutType.TwoColumnsMidExpanded);
            // this.getView().rerender();
            this._triggerCompIconFormatter();

        },

        _cleanUp: function () {
            // var oViewSett = this.getView().getModel("viewData");
            // oViewSett.setData(null);
            this.getView().byId("IndicatorsPanel").removeAllContent();
        },


        _composeUIForCompetencyAppraisal: function (oCompContext) {
            var sCompetencyPath = "SurveyData>" + oCompContext.getPath(),
                oPanel = this.getView().byId("IndicatorsPanel");
            // sPathMobile = "SurveyData>" + oCompContext.getPath() + "/Indicators",

            oPanel.destroyContent();

            var oFragmentInd = this.getIndicatorsFragment(oCompContext),
                oFragmentComp = this.getCompetenceFragment(oCompContext),
                oFragmentComments = this.getCommentsFragment(),
                oCompDescr = new sap.m.Text({
                    text: '{SurveyData>DESCRIPTION}'
                }).addStyleClass('zDescription');

            this._adjustDataBeforeSurveyComposition();
            this._setTexts(oCompContext);

            oPanel.bindElement(sCompetencyPath);

            oPanel.addContent(oCompDescr);
            if (this._hasIndicatorsToShow(oCompContext)) {
                oPanel.addContent(oFragmentInd);
            }
            if (!this.getView().getModel("SurveyData").getProperty("/IN_AVERAGE")) {
                oPanel.addContent(oFragmentComp);
            }
            oPanel.addContent(oFragmentComments);
        },

        _hasIndicatorsToShow: function (oCompContext) {
            var sIndicPath = oCompContext.getPath() + "/Indicators",
                aIndic = oCompContext.getModel("SurveyData").getProperty(sIndicPath),
                bHasIndicators = true;
            if (aIndic.length == 0) {
                bHasIndicators = false;
            }
            return bHasIndicators;
        },

        _setTexts: function (oCompContext) {
            var oCompParams = oCompContext.getModel().getProperty(oCompContext.getPath());
            this.getView().byId("iCompetencyName").setText(oCompParams.NAME);
            this.getView().byId("i0pName").setText(oCompParams.BLOCK_NAME);
        },

        _adjustDataBeforeSurveyComposition: function (sIndScaleType) {
            sIndScaleType = sIndScaleType || Config.indicatorsFragmentDesktop.Table;
            if (sIndScaleType === Config.indicatorsFragmentDesktop.Table) {
                var aIndScaleVals = this.getView().getModel("SurveyData").getProperty("/IndicatorScaleValues/"),
                    oColumnsModel = new JSONModel(),
                    aColumns = $.extend([], aIndScaleVals);

                aColumns.unshift({DESCRIPTION: "Индикаторы"});

                if (this.getView().getModel("SurveyData").getProperty("/INDIC_REJECT")) {
                    aColumns.push({DESCRIPTION: "Не могу оценить"});
                }

                oColumnsModel.setData(aColumns);
                this.getView().setModel(oColumnsModel, "IndicatorsTableColumns");
            }
        },

        getIndicatorsFragment: function (oCompContext) {

            var sScale = oCompContext.getModel().getProperty("/IndicatorScale/SCALE_TYPE"),
                sFragmentName = Config.indicatorsFragmentDesktop[sScale];
            var oFragment = sap.ui.xmlfragment(sFragmentName, this);
            return oFragment;
        },

        getCompetenceFragment: function (oCompContext) {

            var sScale = oCompContext.getModel().getProperty("/CompetencyScale/SCALE_TYPE"),
                sFragmentName = Config.competenceFragmentDesktop[sScale];
            var oFragment = sap.ui.xmlfragment(sFragmentName, this);
            return oFragment;

        },

        getCommentsFragment: function () {
            var sFragmentName = "zhcm_Hap_650.fragments.SurveyComments";
            var oCommentsFragm = sap.ui.xmlfragment(sFragmentName, this);

            return oCommentsFragm;
        },

        onCompetencySliderCantEstimate: function (oEvt) {
            var oCont = oEvt.getSource().getBindingContext("SurveyData");
            var oCompetency = oCont.getModel().getProperty(oCont.getPath());
            oCompetency._MARK_FLOAT = parseFloat(0);
        },

        onIndicSliderCantEstimate: function (oEvt) {
            var oCont = oEvt.getSource().getBindingContext("SurveyData"),
                oIndic = oCont.getModel().getProperty(oCont.getPath()),
                sCompPath = oCont.getPath().split("/Indicators")[0],
                oComp = oCont.getModel().getProperty(sCompPath),
                sAvrgPath = sCompPath + "/_AverageVal";

            oIndic.M_VALUE = parseFloat(0);
            oCont.getModel().setProperty(sAvrgPath, this._calcAverage(oComp));
        },

        showUserGuide: function (oEvt) {
            if (!this._oPopover) {
                // перенесено в _parseSurveyData
                // var oViewDataModel = this.getView().getModel("viewData"),
                //     sIdEvalProc = oViewDataModel.getProperty("/IdEvalProc");
                // oViewDataModel.setProperty("/InstructionButton", DataAccess.getInstrText.call(this, 'SU', 'BTN_POPUP', sIdEvalProc));

                this._oPopover = sap.ui.xmlfragment("zhcm_Hap_650.fragments.UserGuidePopover", this);
                this.getView().addDependent(this._oPopover);
                this._oPopover.attachAfterClose(function () {
                    this._oPopover.destroy();
                    this._oPopover = null;
                }, this);

                this._oPopover.openBy(oEvt.getSource());

            } else {

                this._oPopover.close();
            }

        },

        _showDetail: function (oItem) {
            var bReplace = !Device.system.phone;
            // set the layout property of FCL control to show two columns
            this.getModel("viewData").setProperty("/layout", "TwoColumnsMidExpanded");
            this.getRouter().navTo("object", {
                objectId: oItem.getBindingContext().getProperty("ObjectID")
            }, bReplace);
        },

        indicatorsTableCellsFactory: function (sId, oContext) {
            var aCells = [],
                that = this,
                bEnabled = this.getView().getModel("viewData").getProperty("/ViewActionsEnabled"),
                mCurrIndic = oContext.getModel().getProperty(oContext.sPath),
                oIndicDescr = new ZClickableText({ text: mCurrIndic.DESCRIPTION })
                    .addStyleClass('zIndicatorsTableDescriptionText')
                // oIndicDescr.ontap = this.onClickTextIndicatorsTableDescription.bind(this);
                    .attachPress(this.onClickTextIndicatorsTableDescription, this);

            aCells.push(oIndicDescr);

            $.each(this.getView().getModel("SurveyData").getProperty("/IndicatorScaleValues/"),
                function (sIndex, oScale) {
                    var tButton = new sap.m.ToggleButton({
                        text: parseInt(oScale.ID, 10),
                        press: that.indicatorsTableButtonPressed.bind(that),
                        pressed: mCurrIndic.Q_VALUE === oScale.ID ? true : false,
                        enabled: bEnabled
                    });
                    tButton.addStyleClass("zhcm_Hap_650TableCellButton");
                    tButton.data("valToSet", oScale.ID);
                    aCells.push(tButton);
                });

            if (this.getView().getModel("SurveyData").getProperty("/INDIC_REJECT")) {
                var nButton = new sap.m.ToggleButton({
                    text: 'N',
                    press: that.indicatorsTableButtonPressed.bind(that),
                    pressed: mCurrIndic.CANT_ESTIMATE ? true : false,
                    enabled: bEnabled
                });
                nButton.addStyleClass("zhcm_Hap_650TableCellButton");
                nButton.data("valToSet", "N");
                aCells.push(nButton);
            }

            var oColumnListItem = new sap.m.ColumnListItem(sId, {cells: aCells});
            return oColumnListItem;

        },

        indicatorsTableButtonPressed: function (oEvt) {
            var sValToSet = oEvt.getParameter("pressed") ? oEvt.getSource().data().valToSet : "0000",
                that = this,
                aCells = oEvt.getSource().getParent().getCells();

            $.each(aCells, function (sIndex, oCell) {
                if (sIndex === 0) {
                    return
                }
                if (oCell.sId != oEvt.getSource().sId) {
                    oCell.setPressed(false);
                }
                if (oCell.sId == oEvt.getSource().sId) {
                    var bCantEstimate = false;
                    if (sValToSet === "N") {
                        sValToSet = "0000";
                        bCantEstimate = true;
                    }
                    var oContext = oEvt.getSource().getParent().getBindingContext("SurveyData"),
                        sCantEstimatePath = oContext.getPath() + "/CANT_ESTIMATE",
                        sRatedPath = oContext.getPath() + "/Q_VALUE",
                        sCompPath = oContext.getPath().split("/Indicators")[0],
                        sAveragePath = sCompPath + "/_AverageVal";

                    oContext.getModel().setProperty(sRatedPath, sValToSet);
                    oContext.getModel().setProperty(sCantEstimatePath, bCantEstimate);
                    var fAverage = that._calcAverage(oContext.getModel().getProperty(sCompPath));
                    oContext.getModel().setProperty(sAveragePath, fAverage);

                }
            });

            this._triggerCompIconFormatter();

        },

        onBtnBackPress: function (oEvt) {
            this.getOwnerComponent().getRouter().navTo("overview");
        },

        _processSend: function (sButton, sRefuseText) {
            var that = this,
                oSendData = this._parseBeforeSend();

            oSendData.BUTTON = sButton;
            oSendData.REFUSE_COMMENT = sRefuseText || "";
            this.getView().setBusy(true);
            this._removeCompsErrorState();

            DataAccess.sendSurvey(oSendData)
                .then(function () {
                        if (oSendData.BUTTON != "") {
                            that.getView().setBusy(false);
                            that.onBtnBackPress();
                        } else { // save without status change
                            that._parseSurveyData(oSendData);
                            sap.m.MessageToast.show("Сохранено");
                        }
                    },
                    function (mError) {
                        oMessagePopover.getModel().setData(mError.messages);
                        that.getView().getModel("viewData").setProperty("/msgNum", mError.messages.length);
                        that._setCompsErrorState(mError.competencies);
                        that._parseSurveyData(oSendData);
                        that.getView().byId("messagePopoverBtn").firePress();
                    });

        },

        _removeCompsErrorState: function () {
            var aCompListItems = this.getView().byId("competenciesList").getItems();
            for (var i = 0; i < aCompListItems.length; i++) {
                aCompListItems[i].removeStyleClass("zCompWithError");
            }
        },

        _setCompsErrorState: function (aCompsWithErrors) {
            aCompsWithErrors = aCompsWithErrors || [];
            var aCompListItems = this.getView().byId("competenciesList").getItems();

            for (var i = 0; i < aCompListItems.length; i++) {
                var sCompId = aCompListItems[i].data().compId;
                if (aCompsWithErrors.indexOf(sCompId) > -1) {
                    aCompListItems[i].addStyleClass("zCompWithError");
                }
            }
        },

        _triggerCompIconFormatter: function () {
            this.getView().getModel("viewData").setProperty("/Dummy", Math.random());
        },

        _parseBeforeSend: function () {
            var oParsed = $.extend({}, this.oView.getModel("SurveyData").getData());


            $.each(oParsed.Competencies, function (iInd, oComp) {
                if (oComp.hasOwnProperty('_MARK_FLOAT')) {
                    if (oParsed.CompetencyScale.DATA_TYPE === 'M') {
                        oComp.MARK = oComp._MARK_FLOAT.toString();
                    }
                    delete oComp._MARK_FLOAT;
                    delete oComp._AverageVal;
                }
                $.each(oComp.Indicators, function (iInd, oIndic) {
                    oIndic.M_VALUE = oIndic.M_VALUE.toString();
                    if (oParsed.INDIC_REJECT &&
                        oParsed.IndicatorScale.SCALE_TYPE == "DragNDrop" &&
                        oIndic.Q_VALUE == "0000") {
                        oIndic.CANT_ESTIMATE = true;
                    }
                });
            });

            oParsed.CompetencyScale.MIN = oParsed.CompetencyScale.MIN.toString();
            oParsed.CompetencyScale.MAX = oParsed.CompetencyScale.MAX.toString();
            oParsed.CompetencyScale.STEP = oParsed.CompetencyScale.STEP.toString();
            oParsed.IndicatorScale.MIN = oParsed.IndicatorScale.MIN.toString();
            oParsed.IndicatorScale.MAX = oParsed.IndicatorScale.MAX.toString();
            oParsed.IndicatorScale.STEP = oParsed.IndicatorScale.STEP.toString();

            return oParsed;

        },
        onSaveAppraisal: function (oEvt) {
            this._processSend("");
        },

        onCompleteAppraisal: function (oEvt) {
            var that = this;

            MessageBox.confirm("Вы уверены, что хотите завершить оценку?", {
                styleClass: "zCustomMessageBox",
                onClose: function (sAction) {
                    sAction === MessageBox.Action.OK ? that._processSend("ZNEXT") : "";

                }
            });
        },

        onDeclineToRate: function () {
            var that = this,
                sTextName,
                sFullText = "",
                aDialogContent = [],
                oPerson = this.getView().getModel("Person").getData(),
                oBundle = this.getView().getModel("i18n").getResourceBundle();

            sTextName = oPerson.PERNR === oPerson.APPRAISER ? "SelfRfs" : "Rfs";
            for (var i = 1; i < 11; i++) {
                sFullText += ' ' + oBundle.getText(sTextName + '.' + i);
            }

            aDialogContent.push(new sap.m.FormattedText({htmlText: sFullText}));
            if (oPerson.PERNR !== oPerson.APPRAISER) {
                aDialogContent.push(new sap.m.TextArea('submitDialogTextarea', {
                    width: '100%',
                    placeholder: 'Укажите причину отказа'
                }));
            }

            var oCancelBtn = new sap.m.Button({
                text: 'Отменить',
                type: "Reject",
                press: function () {
                    dialog.close();
                }
            });

            var dialog = new sap.m.Dialog({
                title: 'Подтверждение',
                type: 'Message',
                content: aDialogContent,
                initialFocus: oCancelBtn,
                beginButton: new sap.m.Button({
                    text: 'Подтвердить',
                    type: "Emphasized",
                    press: function () {
                        var oTextArea = sap.ui.getCore().byId('submitDialogTextarea');
                        var sText = oTextArea ? oTextArea.getValue() : '';

                        dialog.close();
                        that._processSend("ZREFUSE", sText);
                    }
                }),
                endButton: oCancelBtn,
                afterClose: function () {
                    dialog.destroy();
                }
            });

            dialog.open();
        },

        handleMessagePopoverPress: function (oEvt) {
            oMessagePopover.toggle(oEvt.getSource());
        },

        onToggleHeader: function () {
            var oViewData = this.getView().getModel("viewData");
            oViewData.setProperty("/HeaderExpanded", !oViewData.getProperty("/HeaderExpanded"));
        },

        onClickTextIndicatorsTableDescription: function (oEvent) {
            // oEvent.stopImmediatePropagation();
            var oTextControl = oEvent.getSource();
            var sDescrText = oTextControl.getText();
            if (!this._oPopoverIndicatorsTableDescription) {
                this._oPopoverIndicatorsTableDescription = new Popover({
                    placement: sap.m.PlacementType.Right,
                    showHeader: false,
                    enableScrolling: false,
                    content: [ new sap.m.Text().addStyleClass("sapUiTinyMargin") ]
                });
            }
            this._oPopoverIndicatorsTableDescription
                .getContent()[0]
                .setText(sDescrText);

            this._oPopoverIndicatorsTableDescription.openBy(oTextControl);
        },


    });
});



// sap.ui.define([
//     "sap/ui/model/CompositeType",
//     // "sap/ui/model/ValidateExample"
// ], function (CompositeType, ValidateExample) {
//     "use strict";

//     return CompositeType.extend("zhcm_IPR.Utils.types.ZSliderValue", {
        
//         parseValue: function (a, b, c) {
//             debugger;
//             return a;
//         },

//         formatValue: function (a, b, c) {
//             debugger;
//             return a;
//         },

//         // validateValue: function (a, b, c) {
//         //     debugger;
//         //     return a;
//         // }

//     });
// });