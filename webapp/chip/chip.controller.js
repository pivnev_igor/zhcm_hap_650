sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function(Controller) {
    "use strict";

    return Controller.extend("ZHCM_PM_650.chip.chip", {
        onInit: function() {
            //ChipSet
            var sServiceUrl = "/sap/opu/odata/SAP/ZHCM_PM_0650_SRV";
            this.oDataModel =
                new sap.ui.model.odata.v2.ODataModel(sServiceUrl, {
                    useBatch: false
                });
            var d = $.Deferred();
            this.oDataModel.read("/ChipSet",{
                success: function(oData, oResponse){
                    d.resolve(oData);
                },
                error: function(oError){
                    d.reject(oError);
                }
            });

            d.then(function(oData){
                var oJSON = new sap.ui.model.json.JSONModel();
                var oChipData = {
                    ActiveTasks : oData.results[0].TASKS_AMOUNT,
                    TasksNumVisible: !!oData.results[0].TASKS_AMOUNT,
                    NoTasksVisible: !oData.results[0].TASKS_AMOUNT,
                    CtrlVisible: !!oData.results[0].VISIBLE,
                    txtInfo: "",
                    txtVisible: false
                };

                oChipData.txtInfo = this._choiceText(oChipData.CtrlVisible, oChipData.TasksNumVisible);
                oChipData.txtVisible = this._textVisible(oChipData.CtrlVisible, oChipData.txtInfo);

                oJSON.setData(oChipData);
                this.getView().setModel(oJSON, "ChipData");
            }.bind(this));

        },

        onAfterRendering: function() {
            $("#" + this.getView().getParent().getId().split("--")[0] + "--laneView--fullscreen").hide();
        },

        onGoToApp: function(oEvent) {
            window.open("/sap/bc/ui5_ui5/sap/ZHCM_PM_650");
        },

        _choiceText: function(isVisible, isTasks) {
            var str = "";
            if (isVisible === false){
                str = "Нет данных";
            } else {
                if (isTasks === false) {
                    str = "Отсутствуют";
                }
            }
            return str;
        },

        _textVisible: function(isVisible, txtInfo)  {
            var bVisible = false;
            if (isVisible === true || txtInfo.length > 0) {
                bVisible = true;
            }
            return bVisible;
        },

    });

});